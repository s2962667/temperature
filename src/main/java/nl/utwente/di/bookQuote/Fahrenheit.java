package nl.utwente.di.bookQuote;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Fahrenheit extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = request.getParameter("celsius") + " Celsius to Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Fahrenheit: " +
                    String.format("%.1f", Double.parseDouble(request.getParameter("celsius")) * 1.8 + 32) +
                "</BODY></HTML>");
  }
}
